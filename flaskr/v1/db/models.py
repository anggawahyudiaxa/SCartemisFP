from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import declarative_base

from v1.db.bootstrap import get_engine

Base = declarative_base()
engine = get_engine()

class TestTable(Base):
    __tablename__ = "test_table"
    __table_args__ = {
        "mysql_engine": "InnoDB"
    }

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(255))

    def __repr__(self) -> str:
        return f"TestTable(id={self.id}), name='{self.name}'"


def create_all_tables():
    Base.metadata.create_all(engine)