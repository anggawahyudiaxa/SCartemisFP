<div align="center" name="top"><h1>Fashion Campus API v.1</h1></div>

<div align="center" name="top"><h3>Startup Campus Final Project</h3></div>

# API Introduction

**Fashion Campus API** is a REST API built on top of the [Flask Framework](https://flask.palletsprojects.com/) using Python 3.

# Introduction

**Fashion Campus** is a Thrift-fashion start-up, kickstarted in the year 2020 in the wake of the COVID-19 pandemic. **Fashion Campus** aims to serve "Indonesian Young Urbans", Indonesian youths aged 15-35 years old. **Fashion Campus** started in 2016 as an offline international multi-brand fashion store, with a spirit to also collaborate with local brands. As "Indonesian Young Urbans" shifted their focus from our old business model, **Fashion Campus** also transformed to fit their needs.

> Manager's Disclaimer:

**Fashion Campus** is a fictional company, serving a fictional market, in a fictional business model, as a mean to fullfill **Startup Campus**' final project. All names, endpoints, features, products, and other functionalities in this project is fictitious. There are NO attempts to portray anything in the real world. Any similarities are purely coincidental

[Startup Campus](https://startupcampus.id/our-story/) is a Certified Independent Study organized by the Achmad Zaky Foundation and fully supported by the Ministry of Education and Culture, Ristekdikti under the auspices of the KAMPUS MERDEKA initiative. Startup Campus provides digital bootcamp with intensive matriculation and real project based for tech skills with job connectors.

# Installation

You can install all of the necessary modules inside `requirements.txt` using the command below in the working directory

```
pip install -r requirements.txt
```

It is also reccomended to use virtual environment, like `venv`. You can install `venv` using the command below

```
py -m venv env
```

And activate the virtual environment by running `env.ps1` (Windows) or running `.\env\Scripts\activate`

# Core API

## Authentication

## Errors

## Universal

| Method | URI                       | Note                     |
| ------ | ------------------------- | ------------------------ |
| `GET`  | `/image/{image_name.ext}` | `Auth-neutral` Get Image |

## Home

| Method | URI              | Note                        |
| ------ | ---------------- | --------------------------- |
| `GET`  | `/home/banner`   | `Auth-neutral` Get Banner   |
| `GET`  | `/home/category` | `Auth-neutral` Get Category |

## Auth

| Method | URI        | Note               |
| ------ | ---------- | ------------------ |
| `POST` | `/sign-up` | `Non-Auth` Sign Up |
| `POST` | `/sign-in` | `Non-Auth` Sign In |

## Product List

| Method | URI                      | Note                                   |
| ------ | ------------------------ | -------------------------------------- |
| `GET`  | `/products`              | `Auth-neutral` Get Product List        |
| `GET`  | `/categories`            | `Auth-neutral` Get Category            |
| `POST` | `/products/search_image` | `Auth-neutral` Search Product by Image |

## Product Details

| Method | URI              | Note                               |
| ------ | ---------------- | ---------------------------------- |
| `GET`  | `/products/{id}` | `Auth-neutral` Get Product Details |
| `POST` | `/cart`          | `Auth` Add to Cart                 |

## Cart

| Method   | URI                      | Note                               |
| -------- | ------------------------ | ---------------------------------- |
| `GET`    | `/cart`                  | `Auth` Get User Cart               |
| `GET`    | `/user/shipping_address` | `Auth` Get User Shipping Address   |
| `GET`    | `/shipping_price`        | `Auth+haveCart` Get Shipping Price |
| `POST`   | `/order`                 | `Auth` Create Order                |
| `DELETE` | `/cart/cart_id`          | `Auth` Delete Cart Item            |

## User Profile

| Method | URI                      | Note                             |
| ------ | ------------------------ | -------------------------------- |
| `GET`  | `/user`                  | `Auth` Get User Details          |
| `POST` | `/user/shipping_address` | `Auth` Change Shipping Address   |
| `PUT`  | `/user/balance`          | `Auth` Top-up Balance            |
| `GET`  | `/user/balance`          | `Auth` Get User Balance          |
| `GET`  | `/user/shipping_address` | `Auth` Get User Shipping Address |
| `GET`  | `/order`                 | `Auth` User Orders               |

## Admin Dash [Admin-only]

| Method | URI             | Note                         |
| ------ | --------------- | ---------------------------- |
| `GET`  | `/admin/orders` | `Auth+Admin` Get Orders      |
| `POST` | `/products`     | `Auth+Admin` Create Product  |
| `POST` | `/categories`   | `Auth+Admin` Create Category |
| `GET`  | `/sales`        | `Auth+Admin` Get Total Sales |
