def return_special(msg, code = 500):
    return {"error": msg}, code

def e_404(e):
    return return_special("Page Not Found!", 404)

def e_500(e):
    return return_special("Internal Server Error", 500)

def e_403(e):
    return return_special("Forbidden!", 403)