import os
from flask import Flask

from v1.config import ConfigDev, ConfigProd
from v1.db.bootstrap import get_engine
from v1.db.models import create_all_tables

from v1.blueprints.test import test_bp
from v1.blueprints.error_handler import e_404, e_403, e_500

def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)

    blueprints = [test_bp]
    for bp in blueprints:
        app.register_blueprint(bp)
    error_handlers = {
        404: e_404,
        403: e_403,
        500: e_500
    }
    for key in error_handlers:
        app.register_error_handler(key, error_handlers[key])

    if test_config is None:
        app.config.from_object(ConfigDev)
    else:
        app.config.from_mapping(test_config)

    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    create_all_tables()

    return app