from os import getenv, path
from dotenv import dotenv_values

config = dotenv_values()

class Config:
    IMAGE_FOLDER="images"
    SECRET_KEY=config["APP_SECRET"]
    DB_CONNECTION=config["DB_CONNECTION"]
    DB_HOST=config["DB_HOST"]
    DB_PORT=config["DB_PORT"]
    DB_DATABASE=config["DB_DATABASE"]
    DB_USERNAME=config["DB_USERNAME"]
    DB_PASSWORD=config["DB_PASSWORD"]

class ConfigProd(Config):
    FLASK_ENV='production'
    DEBUG=False
    TESTING=False

class ConfigDev(Config):
    FLASK_ENV='development'
    DEBUG=True
    TESTING=False