from flask import Blueprint, request, current_app
from sqlalchemy import select
from sqlalchemy.orm import Session

from v1.db.models import TestTable
from v1.db.bootstrap import get_engine, run_query

test_bp = Blueprint("test", __name__, url_prefix="/test")

@test_bp.route('/')
def test_index():
    return {
        "message": "We are testing!",
    }

@test_bp.route('/environ')
def test_environ():
    envs = {}
    for key in current_app.config:
        envs[key] = str(current_app.config[key])
    return {
        "message": "<hidden>"
    }

@test_bp.route('/sql')
def test_sql():
    message = "Everything is OK"
    with Session(get_engine()) as session:
        run_query(f"TRUNCATE TABLE {TestTable.__tablename__}", True)
        names = ["Jamal", "Chris", "Bob", "Johnson"]
        for name in names:
            session.add(
                TestTable(name=name)
            )
        session.commit()
        session.delete(session.scalar(select(TestTable).where(TestTable.name == "Bob")))
        session.commit()

        asserter = {}
        res1 = session.scalars(select(TestTable))
        for row in res1:
            asserter[row.id] = row.name
            
        expected = {
            1: "Jamal",
            2: "Chris",
            4: "Johnson"
        }

        if asserter != expected:
            message = {
                "error": "Test result failed assertion",
                "yours": asserter,
                "expected": expected,
            }

    return {
        "message": message,
    }